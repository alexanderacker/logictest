mod example {
    crepe::crepe! {
        @input
        struct Word<'a>(&'a str);
        @output
        #[derive(Debug)]
        struct Suffix<'a>(&'a str);

        Suffix(w) <- Word(w);
        Suffix(&w[1..]) <- Suffix(w), (!w.is_empty());

        @input
        struct Edge(i32, i32);
        @output
        #[derive(Debug)]
        struct Tc(i32, i32);

        Tc(x ,y) <- Edge(x, y);
        Tc(x, z) <- Edge(x, y), Tc(y, z);
    }

    pub fn run() {
        let mut runtime = Crepe::new();
        runtime.extend(&[Word("banana"), Word("bandana")]);
        runtime.extend(&[Edge(0, 1), Edge(1, 2), Edge(2, 3), Edge(3, 4), Edge(2, 5)]);
        let (suffixes, tcs) = runtime.run();
        println!("{:?}", suffixes);
        for suffix in suffixes {
            println!("{}", suffix.0);
        }

        for tc in tcs {
            println!("{:?}", tc);
        }
    }
}

mod parents {
    crepe::crepe! {
        @input
        struct Parent<'a>(&'a str, &'a str);
        @output
        #[derive(Debug)]
        struct Grandparent<'a>(&'a str, &'a str);
        Grandparent(grandparent, child) <- Parent(grandparent, parent), Parent(parent, child);
    }

    pub fn run() {
        let mut runtime = Crepe::new();
        runtime.extend(&[
            Parent("Adam", "Abel"),
            Parent("Eve", "Abel"),
            Parent("Adam", "Cain"),
            Parent("Eve", "Cain"),
            Parent("Adam", "Seth"),
            Parent("Eve", "Seth"),
            Parent("Seth", "Enos"),
            Parent("Cain", "Enoch"),
        ]);
        let (grandparents,) = runtime.run();
        for grandparent in grandparents {
            println!("{} is a grandparent of {}", grandparent.0, grandparent.1);
        }
    }
}

mod nouns {
    #[derive(Debug, PartialEq, Eq, Hash)]
    enum Case {
        Nominative,
        Genitive,
        Dative,
        Accusative,
        Ablative,
        Vocative,
        Locative,
    }

    #[derive(Debug, PartialEq, Eq, Hash)]
    enum Number {
        Singular,
        Plural,
    }

    #[derive(Debug, PartialEq, Eq, Hash)]
    enum Gender {
        Masculine,
        Feminine,
        Neuter,
    }

    #[derive(Debug, PartialEq, Eq, Hash)]
    enum Declension {
        First,
        SecondUs,
        SecondIus,
        SecondR,
        SecondRContract,
        SecondUm,
    }

    crepe::crepe! {
        @input
        struct Noun<'a>(&'a str);

        @output
        #[derive(Debug)]
        struct NounInflectSuccess<'a>(&'a str, &'a Case, &'a Number, &'a Gender, &'a Declension);

        struct Suffix<'a>(&'a str, &'a str);
        struct NounInflection<'a>(&'a str, &'a Case, &'a Number, &'a Gender, &'a Declension);

        Suffix(n, n) <- Noun(n);
        Suffix(n, &w[1..]) <- Suffix(n, w), (!w.is_empty());

        NounInflectSuccess(suffix, case, num, gender, decl) <- Noun(n), Suffix(n, suffix), NounInflection(suffix, case, num, gender, decl);


        NounInflection("a", &Case::Nominative, &Number::Singular, &Gender::Feminine, &Declension::First);
        NounInflection("ae", &Case::Genitive, &Number::Singular, &Gender::Feminine, &Declension::First);
        NounInflection("ae", &Case::Dative, &Number::Singular, &Gender::Feminine, &Declension::First);
        NounInflection("am", &Case::Accusative, &Number::Singular, &Gender::Feminine, &Declension::First);
        NounInflection("am", &Case::Ablative, &Number::Singular, &Gender::Feminine, &Declension::First);
        NounInflection("a", &Case::Vocative, &Number::Singular, &Gender::Feminine, &Declension::First);

        NounInflection("ae", &Case::Nominative, &Number::Singular, &Gender::Feminine, &Declension::First);
        NounInflection("arum", &Case::Genitive, &Number::Singular, &Gender::Feminine, &Declension::First);
        NounInflection("is", &Case::Dative, &Number::Singular, &Gender::Feminine, &Declension::First);
        NounInflection("as", &Case::Accusative, &Number::Singular, &Gender::Feminine, &Declension::First);
        NounInflection("is", &Case::Ablative, &Number::Singular, &Gender::Feminine, &Declension::First);
        NounInflection("a", &Case::Vocative, &Number::Singular, &Gender::Feminine, &Declension::First);
    }

    pub fn run() {
        let mut runtime = Crepe::new();
        runtime.extend(&[Noun("mensa")]);
        let (infls,) = runtime.run();
        for infl in infls {
            println!("Matched: {:?}", infl);
        }
    }
}

fn main() {
    nouns::run();
}
